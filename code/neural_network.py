# import statements
import pickle
import time
from random import choice
from keras.optimizers import SGD
from scipy.stats import variation
import pandas as pd
from sklearn.preprocessing import LabelEncoder, LabelBinarizer
from sklearn.preprocessing import StandardScaler
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation
from keras.utils import np_utils
from keras.callbacks import EarlyStopping
from sklearn.model_selection import GridSearchCV, train_test_split
from keras.wrappers.scikit_learn import KerasClassifier
import numpy as np
from matplotlib import pyplot


def data_cleaning(data_filename):
    intermediate = open(data_filename, 'rb')
    data_set = pickle.load(intermediate)
    indexes = data_set.index.values.tolist()
    cleaner = lambda x: " ".join([x.split()[0][1:-1], x.split()[1]]) \
        if "[" in x and "]" in x else x
    indexes = list(map(cleaner, indexes))
    indexes = pd.Series(indexes)
    data_set.set_index(indexes, inplace=True)
    # print('==========================Removing species occurring only once in the data.............')
    unique_specie_in_data = data_set.index.value_counts() == 1
    data_set.drop(data_set[unique_specie_in_data].index, inplace=True)
    data_set = data_set.fillna(0)
    col_with_mean_zero = data_set.columns[data_set.sum(axis=0) == 0]
    data_set.drop(col_with_mean_zero, axis=1, inplace=True)
    # print('==========================Removing columns with zero mean.............')
    print(f'There are {len(unique_specie_in_data)} species with one count')
    print(f"The shape of the data_set is {data_set.shape[0]} rows x {data_set.shape[1]} columns")
    return data_set


def number_of_neuron_generator(number_features, num_classes, number_layers):
    neuron_per_layer = []
    guide = number_features - 100
    for i in range(number_layers):
        num_neuron = choice(list(range(num_classes, guide + 1)))
        neuron_per_layer.append(num_neuron)
    return neuron_per_layer


def is_valid(number_features, num_classes, number_neuron_list):
    threshold = (2 * number_features / 3) + num_classes
    for i in range(len(number_neuron_list) - 1):
        if number_neuron_list[i] <= number_neuron_list[i + 1]:
            return False
        if number_neuron_list[-1] - num_classes <= 50:
            return False
    if sum(number_neuron_list) <= threshold:
        return False
    return True


def hidden_layer_and_definition(n_features, n_classes, number_of_layer):
    neuron_layer = None
    valid = False
    while not valid:
        neuron_layer = number_of_neuron_generator(n_features, n_classes, number_of_layer)
        valid = is_valid(n_features, n_classes, neuron_layer)
    return neuron_layer


def create_model(num_features, n_classes, alpha, hidden_neuron, dropout_rate=0.0):
    model_to_be_fitted = Sequential()
    model_to_be_fitted.add(Dense(hidden_neuron[0], input_dim=num_features, activation='relu'))
    model_to_be_fitted.add(Dropout(dropout_rate))
    if len(hidden_neuron[1:]) != 0:
        for element in hidden_neuron[1:]:
            model_to_be_fitted.add(Dense(element, activation='relu'))
            model_to_be_fitted.add(Dropout(dropout_rate))
    model_to_be_fitted.add(Dense(450, activation='softmax'))
    # model.compile(loss='categorical_crossentropy', optimizer='rmsprop', metrics=['accuracy'])
    sgd = SGD(lr=alpha, decay=1e-6, momentum=0.9, nesterov=True)
    model_to_be_fitted.compile(loss='categorical_crossentropy', optimizer=sgd,
                               metrics=['accuracy'])
    return model_to_be_fitted


file = "all_samples_info_df0.pickle"
data1 = data_cleaning(file)
inputs, labels = data1.values, list(data1.index.values)
x_train, x_test, y_train, y_test = train_test_split(inputs, labels, test_size=0.25)
n_classes = len(data1.index.unique())
encoder = LabelBinarizer()
labels_enc_train = encoder.fit_transform(y_train)
test_label_enc = encoder.fit_transform(y_test)
x_train_t = StandardScaler().fit_transform(x_train)
x_test_t = StandardScaler().fit_transform(x_test)

y_train_t = np_utils.to_categorical(labels_enc_train)
y_test_t = np_utils.to_categorical(test_label_enc)
print(x_train.shape)
input_dim = x_train.shape[1]

param_grid = dict(
    alpha=[0.001],  # , 0.005, 0.007, 0.009], # 0.01, 0.03, 0.05, 0.07, 0.09, 0.1, 0.2, 0.3, 0.4, 0.5],
    dropout_rate=[0.0],  # 0.4, 0.3, 0.5],
    hidden_neuron=[hidden_layer_and_definition(2771, n_classes, k) for k in range(1, 3)],
    n_classes=[n_classes],
    nb_epoch=[100],  # 1000, 1500],
    batch_size=[50, 100],  # 200, 300, 400, 500]
)


def parameters_optimization(x_train, y_tain, grid, model_function):
    start = time.time()
    classifier = KerasClassifier(build_fn=model_function, verbose=0)
    grid = GridSearchCV(estimator=classifier, param_grid=param_grid,
                        n_jobs=-1, verbose=1, scoring='neg_log_loss')
    grid_result = grid.fit(x_train_t, y_train_t)
    print("Best: %f using %s" % (grid_result.best_score_, grid_result.best_params_))
    means = grid_result.cv_results_['mean_test_score']
    stand = grid_result.cv_results_['std_test_score']
    params = grid_result.cv_results_['params']
    for mean, stdev, param in zip(means, stand, params):
        print("%f (%f) with: %r" % (mean, stdev, param))
    duration = time.time() - start
    return grid_result, duration


i = parameters_optimization(x_train_t, y_train_t, param_grid, create_model)


def model_fit(features, outputs, best_params_, n_iterations, preprocess=False):
    t0 = time.time()
    list_accuracy = np.zeros(n_iterations + 1)
    for i in range(n_iterations):
        if i % 10 == 0:
            print(f"Doing {i}th iteration........")

        mod = svm.SVC(kernel='linear', C=100, gamma=0.0001)
        if preprocess == False:
            x_training, x_test, y_training, y_test = train_test_split(x, label, test_size=0.25)
            fitting = mod.fit(x_training, y_training)
            list_accuracy[i] = fitting.score(x_test, y_test)
        elif preprocess == True:
            x_standardized = preprocessing.scale(x)
            x_training, x_test, y_training, y_test = train_test_split(x_standardized, label, test_size=0.25)
            fitting = mod.fit(x_training, y_training)
            list_accuracy[i] = fitting.score(x_test, y_test)
    t1 = time.time()
    list_accuracy[-1] = t1 - t0
    return list_accuracy


model = create_model(num_features=input_dim, n_classes=n_classes, alpha=0.005,
                     hidden_neuron=[2400],
                     dropout_rate=0.2)
# Train on all data using early stopping to prevent overfitting
early_stopper = EarlyStopping(monitor='val_loss', patience=2, verbose=1, mode='auto')
model.fit(x_train, labels_enc_train, nb_epoch=500,
          batch_size=50, verbose=0, callbacks=[early_stopper])

# evaluate the model
_, train_acc = model.evaluate(trainX, trainy, verbose=0)
_, test_acc = model.evaluate(testX, testy, verbose=0)
print('Train: %.3f, Test: %.3f' % (train_acc, test_acc))
# learning curves of model accuracy
pyplot.plot(history.history['accuracy'], label='train')
pyplot.plot(history.history['val_accuracy'], label='test')
pyplot.legend()
pyplot.show()

y_pred = model.predict_proba(x_test_t, verbose=0)
for i in range(len(x_test_t)):
    print("X=%s, Predicted=%s" % (x_test_t[i], y_test_t[i]))

pred = encoder.inverse_transform(y_pred)
tes = encoder.inverse_transform(y_test)
ft = np.array([tes, pred])
fe = {'pred': pred, 'test': tes}
e = pd.DataFrame(fe)

y_pred_classes = model.predict_classes(x_test_t, verbose=0)
y_pred_classes = y_pred_classes.reshape(1, 932).ravel()
acc = accuracy_score(test_label_enc, y_pred_classes, normalize=True)
precision = precision_score(test_label_enc, y_pred_classes, average='macro')
recall = recall_score(test_label_enc, y_pred_classes)
f1 = f1_score(test_label_enc, y_pred_classes)
print(f'Accuracy: {acc} \n Presision: {precision} \n recall: {recall} \n f1: {f1}')
f = encoder.inverse_transform(y_pred_classes)

if __name__ == '__main__':
    data_file = "all_samples_info_df0.pickle"
    data = data_cleaning(data_file)

    sum_per_column = data.sum(axis=0)
