# import statements for Spark session, Pipeline, Functions and Metrics
from pyspark import SparkContext, SparkConf
from pyspark.sql import SQLContext
from pyspark.ml.feature import OneHotEncoderEstimator, StringIndexer, StandardScaler, VectorAssembler
from pyspark.ml import Pipeline
from pyspark.sql.functions import rand
from pyspark.mllib.evaluation import MulticlassMetrics

# import statements for keras / Deep Learning
from keras.models import Sequential
from keras.layers.core import Dense, Dropout, Activation
from keras import optimizers, regularizers
from keras.optimizers import Adam


# import statement for Deep Learning on Spark
from elephas.ml_model import ElephasEstimator


# Start spark session
conf = SparkConf().setAppName("Spark Deep Learning Pipeline").setMaster("local[6]")
sc = SparkContext(conf=conf)
sql_context = SQLContext(sc)
geno_df = sql_context.read.csv("all_genomes.csv", header=True, inferSchema=True)
