import pickle
import pandas as pd
from SPARQLWrapper import SPARQLWrapper, JSON
import os


def retrieve_phenotype_information(phenotypes, endpoint, phenotype_keyword):
    assert (len(phenotypes) == len(phenotype_keyword)), \
        "Make sure to provide a keyword for each phenotype"
    for i in range(len(phenotypes)):
        phenotype_data = []
        query = """
        SELECT ?accession ?nameLabel ?%sLabel
        WHERE{
                ?entry wdt:P7 ?accession;
                        wdt:P41 ?name;
                        %s ?%s.
                SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en"}
                }""" % (phenotype_keyword[i], phenotypes[i], phenotype_keyword[i])

        print("Getting phenotype data for", phenotype_keyword[i])

        sparql = SPARQLWrapper(endpoint)
        sparql.setQuery(query)
        sparql.setReturnFormat(JSON)
        results = sparql.query().convert()

        for result in results["results"]["bindings"]:
            phenotype_data.append([result["accession"]["value"],
                                   result["nameLabel"]["value"],
                                   result[f"{phenotype_keyword[i]}Label"]["value"]])
        if os.path.exists(f"{phenotype_keyword[i]}.csv"):
            print("Done! File already exists!")
        else:
            data = pd.DataFrame(data=phenotype_data, columns=["accession", "name", f"{phenotype_keyword[i]}"])
            data.to_csv(f"{phenotype_keyword[i]}.csv", index=True, sep=',')
            print("Done! Phenotype file has been writing out")
        print(f"There are {pd.read_csv(f'{phenotype_keyword[i]}.csv').shape[0]} lines of phenotype data available\n")

def
"""

def data_cleaning(data_filename):
    intermediate = open(data_filename, 'rb')
    data_set = pickle.load(intermediate)
    indexes = data_set.index.values.tolist()
    cleaner = lambda x: " ".join([x.split()[0][1:-1], x.split()[1]]) \
        if "[" in x or "]" in x else x
    indexes = list(map(cleaner, indexes))
    indexes = pd.Series(indexes)
    data_set.set_index(indexes, inplace=True)
    # print('==========================Removing species occurring only once in the data.............')
    unique_specie_in_data = data_set.index.value_counts() == 1
    data_set.drop(data_set[unique_specie_in_data].index, inplace=True)
    data = data_set.fillna(0)
    # print('==========================Removing columns with zero mean.............')
    print(f'There are {len(unique_specie_in_data)} species with one count')
    print(f"The shape of the data_set is {data.shape[0]} rows x {data.shape[1]} columns")
    return data_set
    """

if __name__ == '__main__':
    q_endpoint = "http://sparql.systemsbiology.nl/proxy/wdqs/bigdata/namespace/wdq/sparql"
    phenotype_predicates = ["wdt:P26", "wdt:P28", "wdt:P29", "wdt:P30", "wdt:P31", "wdt:P32",
                            "wdt:P33", "wdt:P36"]
    phenotype_descriptions = ["oxyRequire", "gramStaining", "salinity", "motility", "tempRange",
                              "optimumViableTemp", "ammoniumOptimum", "pathogenicity"]
    retrieve_phenotype_information(phenotype_predicates, q_endpoint, phenotype_descriptions)
